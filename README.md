# OpenCraft Helm Charts

This repository collects the OpenCraft Helm Charts.

## Prerequisites

* Install [Helm](https://helm.sh/docs/intro/quickstart/)
* Install [Helm push plugin](https://github.com/chartmuseum/helm-push)
* Run `helm repo add --username <username> --password <access_token> opencraft-helm-charts https://gitlab.com/api/v4/projects/36348812/packages/helm/stable`, where `<username>` and `<password>` are your GitLab credentials, to add the opencraft-helm-charts Helm repository.

## Updating chart dependencies

To update a chart dependency, bump the dependency's version number to the desired version, and execute `helm dependency update "<chart>"`, where `<chart>` is the name of your chart. To update all charts but the library, you can run `ls -1 | grep -v libdjango | xargs -n1 helm dependency update`.

Charts are stored on GitLab. to publish a new version follow the steps below:

1. Ensure that the chart version and/or application version is updated in Chart.yaml
2. Run `helm dependency update <chartname>`
3. Run `helm cm-push <chartname> opencraft-helm-charts`, where `<chartname>` is the chart's directory name

After pushing, you may want to run `helm repo update` to refresh the helm repo state.

## Using the libdjango Helm library

For more information, please check the library's [README.md](libdjango/README.md).

# <appname> Helm Chart

This is the helm chart of https://gitlab.com/opencraft/dev/<appname>.

For more information about the application, please read its README.md.

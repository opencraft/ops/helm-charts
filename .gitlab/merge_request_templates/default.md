## Description

_Describe what this pull request changes, and why. Include implications for people using this change._

## Supporting information

_Link to other information about the change, such as GitLab issues, GitHub issues, forum discussions. Be sure to check they are publicly readable, or if not, repeat the information here._

## Testing instructions

Steps to test the changes:

1. _TBD_

## Dependencies

_List the dependencies required for this change, if any. Do not forget to link [grove-template](https://gitlab.com/opencraft/dev/grove-template) merge request here if that's affected by this change._

## Screenshots

_If applicable, add screenshots to help explain your feature._

## Checklist

If any of the items below is not applicable, do **not** remove them, but put a check in it.

- [ ] Git commit history is clean
- [ ] Git commits are following conventional-commit practices
- [ ] Chart dependencies are updated (`helm dependency update <chart>`)
- [ ] Chart versions are bumped
- [ ] Application version is bumped
- [ ] Documentation is added/updated

## Additional context

_Add any other context about the merge request here._

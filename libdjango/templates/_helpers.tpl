{{/*
Expand the name of the chart.
*/}}
{{- define "libdjango.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "libdjango.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "libdjango.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "libdjango.labels" -}}
{{ include "libdjango.selectorLabels" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "libdjango.selectorLabels" -}}
app.kubernetes.io/name: {{ include "libdjango.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Version labels. These labels should not be defined in selector labels
as immutability leads to issues on upgrading Chart version
reference: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#label-selector-updates
*/}}
{{- define "libdjango.versionLabels" -}}
{{- if .Chart.AppVersion }}
helm.sh/chart: {{ include "libdjango.chart" . }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end}}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "libdjango.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "libdjango.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- /*
libdjango.merge will merge two YAML templates and output the result.
This takes an array of three values:
- the top context
- the template name of the overrides (destination)
- the template name of the base (source)
*/}}
{{- define "libdjango.merge" -}}
{{- $top := first . -}}
{{- $overrides := fromYaml (include (index . 1) $top) | default (dict ) -}}
{{- $tpl := fromYaml (include (index . 2) $top) | default (dict ) -}}
{{- toYaml (merge $overrides $tpl) -}}
{{- end -}}

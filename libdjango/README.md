# Libdjango Helm Library

This Helm library serves as a base for all Django application deployed by OpenCraft to  Kubernetes.

The front-end code may be deployed differently, depending on the service. For more information about how the front-end is deployed for the given application, please check the application's readme.

## Usage

Helm libraries are templates that can be included and extended by application charts. The `libdjango` library defines all templates that may be needed for a Django application we run. Since most components are independent, you can use only what you need.

Generally speaking, do the following to start using the library from scratch:

1. Clone the repository `git clone git@gitlab.com:opencraft/ops/helm-charts.git`
2. Run `cd helm-charts`
3. Run `cp -R example <appname>` where `<appname>` is the name of the new chart
4. Update `<appname>/Chart.yaml` and `<appname>/values.yaml` based on your needs. Also, replace `<appname>` with the name of the app, eg: `crafty-bot`.
5. Delete any files/directories from `<appname>/templates` that are not needed

## Values

The `values.yaml` file, which has all configuration options, is **not parsed** for libraries. Therefore, it serves only as a reference for those who want to use the library.

## Templates

The library provides templates for the following features:

* Django application
* Static file collection job for collecting static files
* Database migrations run job for executing database migrations
* Optional application autoscaling based on RAM/CPU usage
* Optional service account creation
* Celery worker setup (per queue) for async tasks
* Celery beat setup for scheduled tasks
* Celery flower setup for monitoring queues
* Redis integration for caching
* Liveness and Readiness probes for the application and flower to ensure the pods are running

Why not exposing flower? It is rarely used. Not exposing it does not mean we cannot connect to it. We can use port-forwarding (`kubectl --namespace <NAMESPACE> port-forward --address 0.0.0.0 deployments/<RELEASE_NAME>-flower 8001:5555`) and connect to http://localhost:8001 to access it. Also, we would have to assign a basic auth for the ingress controller of the flower service to secure it which makes the debugging slower.

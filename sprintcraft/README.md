# Sprintcraft Helm Chart

This is the helm chart of https://gitlab.com/opencraft/dev/sprintcraft.

For more information about the application, please read its README.md.

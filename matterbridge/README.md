# Matterbridge Helm Chart

This is a helm chart for https://github.com/42wim/matterbridge.

For more information about the application, please read its README.md.

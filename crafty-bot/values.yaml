nameOverride: ""
fullnameOverride: ""

image:
  pullPolicy: IfNotPresent
  imagePullSecrets: [ ]
  repository: "registry.gitlab.com/opencraft/dev/crafty-bot"
  # Overrides the image tag whose default is the chart appVersion.
  tag: ""

configmap:
  data:
    COMPRESS_ENABLED: "true"
    DJANGO_ACCOUNT_ALLOW_REGISTRATION: "false"
    DJANGO_SECURE_SSL_REDIRECT: "false"
    DJANGO_SERVER_EMAIL: "" # We don't send any emails
    DJANGO_SETTINGS_MODULE: config.settings.production

# NOTE: This is filled by Terraform
secrets:
  data: { }

serviceAccount:
  create: false
  # Annotations to add to the service account
  annotations: { }
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

# Django application configuration
django:
  command: [
    "gunicorn",
    "--workers", "4",
    "--bind", ":5000",
    "--log-level", "INFO",
    "config.wsgi",
  ]

  manage:
    collectStatic: false
    migrate: false

  replicaCount: 1

  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 5
    targetCPUUtilizationPercentage: 80
    targetMemoryUtilizationPercentage: 80

  service:
    type: ClusterIP
    port: 5000
    livenessProbePath: /
    readinessProbePath: /

  # NOTE: This is filled by Terraform
  ingress:
    enabled: true
    className: "nginx"
    corsEnabled: true
    annotations: { }
    notificationEmail: "ops@opencraft.com"
    hosts: []
      # - host: django.local
      #   port: 80
      #   paths:
      #     - path: /
      #       pathType: ImplementationSpecific
    tls: []
      # - secretName: "ingress"
      #   hosts: []


  podSecurityContext: { }
  securityContext: { }
  resources:
    limits:
      cpu: 0.5
      memory: 1536Mi
    requests:
      cpu: 0.05
      memory: 350Mi
  nodeSelector: { }
  tolerations: [ ]
  affinity: { }

# Celery worker configuration
celeryWorker:
  queues:
    - name: celery
      replicaCount: 1
      command: [
        "celery", "worker",
        "-A", "config.celery_app",
        "-l", "info",
        "-Q", "celery",
      ]
      podSecurityContext: { }
      securityContext: { }
      resources:
        limits:
          cpu: 0.5
          memory: 700Mi
        requests:
          cpu: 0.05
          memory: 200Mi
      nodeSelector: { }
      tolerations: [ ]
      affinity: { }

# Celery beat configuration
celeryBeat:
  command: [
    "celery", "beat",
    "-A", "config.celery_app",
    "-l", "info",
  ]

  podSecurityContext: { }
  securityContext: { }
  resources:
    limits:
      cpu: 0.1
      memory: 250Mi
    requests:
      cpu: 0.025
      memory: 150Mi
  nodeSelector: { }
  tolerations: [ ]
  affinity: { }

# Flower application configuration
flower:
  command: [
    "celery", "flower",
    "-A", "config.celery_app",
    "--port=5555",
    "--address=0.0.0.0",
  ]

  service:
    type: ClusterIP
    port: 5555
    livenessProbePath: /
    readinessProbePath: /

  podSecurityContext: { }
  securityContext: { }
  resources:
    limits:
      cpu: 0.1
      memory: 250Mi
    requests:
      cpu: 0.025
      memory: 150Mi
  nodeSelector: { }
  tolerations: [ ]
  affinity: { }

redis:
  image:
    repository: redis
    tag: 7.0.0

  replicaCount: 1

  podSecurityContext: { }
  securityContext: { }
  resources:
    limits:
      cpu: 0.25
      memory: 1536Mi
    requests:
      cpu: 0.035
      memory: 10Mi
  nodeSelector: { }
  tolerations: [ ]
  affinity: { }

# CraftyBot Helm Chart

This is the helm chart of https://gitlab.com/opencraft/dev/crafty-bot.

For more information about the application, please read its README.md.
